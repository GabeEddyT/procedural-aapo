﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;


public class Concatonator : MonoBehaviour
{
    public Transform pose0, pose1;

    List<Transform> resultChildren, children0, children1;

    // Start is called before the first frame update
    void Start()
    {
        resultChildren = GetComponentsInChildren<Transform>().ToList();
        children0 = pose0.GetComponentsInChildren<Transform>().ToList();
        children1 = pose1.GetComponentsInChildren<Transform>().ToList();
    }

    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < resultChildren.Count; i++)
        {
            resultChildren[i].localPosition = children0[i].localPosition + children1[i].localPosition;
            resultChildren[i].localEulerAngles = children0[i].localEulerAngles + children1[i].localEulerAngles;

        }
    }
}
