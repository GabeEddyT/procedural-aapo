﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Driver : MonoBehaviour
{
    Interpolator myInterpolator;
    
    [Range(0.01f, 8f)]
    public float scale = 1.0f;

    float t, prevT;
    // Start is called before the first frame update
    void Start()
    {
        myInterpolator = GetComponent<Interpolator>();
    }

    // Update is called once per frame
    void Update()
    {
        t = myInterpolator.t = Time.time % (1/scale) * (scale);
        myInterpolator.offset = t < prevT ? (myInterpolator.offset + 1) % 4 : myInterpolator.offset;

        prevT = t;
    }
}
