﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Interpolator : MonoBehaviour
{
    public enum BlendMethod
    {
        Catmull,
        Bezier,
        Lerp
    }

    public BlendMethod blend;

    public Transform[] parentKeys;
    [Range(0,1)]
    public float t;

    [Range(0,3)]
    public int offset = 0;

    public Transform[] children;

    public List<Transform> [] childKeys = new List<Transform>[4];
    
    // Start is called before the first frame update
    void Start()
    {
        children = GetComponentsInChildren<Transform>();
        foreach(var (parentKey, index) in parentKeys.Select((v, i) => (v, i)))
        {
            childKeys[index] = parentKey.GetComponentsInChildren<Transform>().ToList();
        }
    }

    // Update is called once per frame
    void Update()
    {
        int offset(int x) => (x + this.offset * (blend == BlendMethod.Bezier ? 2 : 1)) % parentKeys.Length;

        for (int i = 0; i < children.Length; i++)
        {
            Interpolate(children[i], new Transform[]{childKeys[offset(0)][i],childKeys[offset(1)][i], childKeys[offset(2)][i], childKeys[offset(3)][i]}, t);
        }        
    }

    Vector4 ToVector (Quaternion q)
    {
        return new Vector4(q.x, q.y, q.z, q.w);
    }

    Quaternion ToQuaternion (Vector4 v)
    {
        return new Quaternion(v.x, v.y, v.z, v.w);
    }

    // https://gamedev.stackexchange.com/a/76102
    Vector4 SO3ToR4(Quaternion q)
    {
        float mul = 1.0f / Mathf.Sqrt(2.0f * (1.0f - q.w));
        return new Vector4(q.x, q.y, q.z, (1.0f - q.w)) * (mul);
    }

    Vector4 mul(Vector4 v0, Vector4 v1)
    {
        return new Vector4(
            v0.x * v1.x,
            v0.y * v1.y,
            v0.z * v1.z,
            v0.w * v1.w);
    }

    float sum(Vector4 v)
    {
        return v.x + v.y + v.z + v.w;
    }

    Quaternion R4ToSO3(Vector4 v)
    {       
        Vector4 sq = mul(v,v);
        float denom = sq.x + sq.y + sq.z + sq.w;
        float s = sq.x + sq.y + sq.z  - sq.w;
        return new Quaternion(
            2.0f * v.x * v.w / denom,
            2.0f * v.y * v.w / denom,
            2.0f * v.z * v.w / denom,
            s / denom);
    }

    // http://archive.gamedev.net/archive/reference/articles/article1497.html
    // http://www.mvps.org/directx/articles/catmull/
    void Interpolate(Transform result, Transform[] keys, float t) 
    {

        // bezier 3rd order
        void bezier()
        {
            result.localRotation = 
                Quaternion.Slerp(
                    Quaternion.Slerp(keys[0].localRotation, keys[1].localRotation, t), 
                    Quaternion.Slerp(keys[1].localRotation, keys[2].localRotation, t), 
                    t);
            result.localPosition =
                Vector4.Lerp(
                    Vector4.Lerp(keys[0].localPosition, keys[1].localPosition, t),
                    Vector4.Lerp(keys[1].localPosition, keys[2].localPosition, t),
                    t);
        }

        void lerp()
        {
            result.localRotation = Quaternion.Slerp(keys[0].localRotation, keys[1].localRotation, t);
            result.localPosition = Vector4.Lerp(keys[0].localPosition, keys[1].localPosition, t);
        }

        // t %= 1;

        // result.localPosition = 
        //     keys[0].localPosition * ((2 * t*t*t) - 3 * (t*t) + 1) + 
        //     keys[3].localPosition * ((3 * t*t) - (2 * t*t*t)) +
        //     keys[1].localPosition * ((t*t*t) - (2 * t*t) + t) +
        //     keys[2].localPosition * (t*t*t - t*t);



        // cubic
        //result.localRotation =
        //   R4ToSO3(SO3ToR4(keys[0].localRotation) * ((2 * t * t * t) - 3 * (t * t) + 1) +
        //   SO3ToR4(keys[3].localRotation) * ((3 * t * t) - (2 * t * t * t)) +
        //   SO3ToR4(keys[1].localRotation) * ((t * t * t) - (2 * t * t) + t) +
        //   SO3ToR4(keys[2].localRotation) * (t * t * t - t * t));


        // cubic - individual component
        /* result.localRotation = new Quaternion(
            keys[0].localRotation.x * ((2 * t*t*t) - 3 * (t*t) + 1) + 
            keys[3].localRotation.x * ((3 * t*t) - (2 * t*t*t)) +
            keys[1].localRotation.x * ((t*t*t) - (2 * t*t) + t) +
            keys[2].localRotation.x * (t*t*t - t*t),
            keys[0].localRotation.y * ((2 * t*t*t) - 3 * (t*t) + 1) + 
            keys[3].localRotation.y * ((3 * t*t) - (2 * t*t*t)) +
            keys[1].localRotation.y * ((t*t*t) - (2 * t*t) + t) +
            keys[2].localRotation.y * (t*t*t - t*t),
            keys[0].localRotation.z * ((2 * t*t*t) - 3 * (t*t) + 1) + 
            keys[3].localRotation.z * ((3 * t*t) - (2 * t*t*t)) +
            keys[1].localRotation.z * ((t*t*t) - (2 * t*t) + t) +
            keys[2].localRotation.z * (t*t*t - t*t),
            1).normalized;     */

        // result.localRotation = Quaternion.Slerp(keys[0].localRotation, keys[3].localRotation,t);

        // catmull
        void catmull () {
            result.localPosition = .5f * (
               2 * keys[1].localPosition +
               (-keys[0].localPosition + keys[2].localPosition) * t +
               (2 * keys[0].localPosition - 5 * keys[1].localPosition + 4 * keys[2].localPosition - keys[3].localPosition) * t * t +
               (-keys[0].localPosition + 3 * keys[1].localPosition - 3 * keys[2].localPosition + keys[3].localPosition) * t * t * t
           );

            // catmull rots
            Vector4[] rots = keys.Select(key => ToVector(key.rotation)).ToArray();

            // https://stackoverflow.com/a/13478876
            // ensure we aren't going the long way around
            if (sum(mul(rots[1], rots[2])) < 0)
            {
                rots[1] *= -1;
            }


            result.rotation = ToQuaternion(.5f * (
                2 * rots[1] +
                (-rots[0] + rots[2]) * t +
                (2 * rots[0] - 5 * rots[1] + 4 * rots[2] - rots[3]) * t * t +
                (-rots[0] + 3 * rots[1] - 3 * rots[2] + rots[3]) * t * t * t
            )).normalized;
        }

        switch (blend)
        {
            case BlendMethod.Bezier:
                bezier();
                break;
            case BlendMethod.Catmull:
                catmull();
                break;
            case BlendMethod.Lerp:
                lerp();
                break;
            default:
                goto case BlendMethod.Catmull;
        }
    }
}
